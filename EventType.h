#ifndef EVENT_TYPE_H__
#define EVENT_TYPE_H__

#include "TObject.h"

class Event: public TObject
{
public:
        int     id;
        double  time;
        std::vector<int> tiles;
        std::vector<double> hits;

ClassDef(Event,1)
};

class Tile: public TObject
{
public:
        int id;
        double  offset;
        double  jitter;

ClassDef(Tile,1)
};

//class PreRow: public TObject
//{
//public:
//        vector<vector<double> >  vvdT;
//
//ClassDef(PreRow,1)
//};

#endif

