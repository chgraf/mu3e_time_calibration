#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "TFile.h"
#include "TTree.h"
#include "TObject.h"
#include "TRandom3.h"
#include "TTreeIndex.h"

#include "EventType.h"

using namespace std;

const char* fDtName = "dTs/dT1e4-10T-n-c.root";
const char* fTilesName = "tiles/tiles10.root";

const double MAX_TIME = 301; //[s]

const double RATE_IC = 1e3; // 1e3[Hz]
const double RATE_e = 0e5;//1e1; // 10e6[Hz]
const double NOISE_RATE = 0.05; //Noise rate
const bool f_noise = false; //Noise rate
const bool f_crosstalk= false; //Noise rate

const int nTiles_phi = 1;  //56
const int nTiles_z = 10;    //60
const int nStations = 1;    //4

int nTiles = nTiles_phi * nTiles_z * nStations;
int nEvents = 1e4;

int nHits = 0;

double Time = 0;

vector<Tile> vTiles; //stores the time of the hits for each tile

//edge tile has distance 0
int tile_dist_from_edge(int tile_id) { 
    int station_id = tile_id / (nTiles_phi*nTiles_z); // / rounds down -> station_id of tile;
    int tile_id_on_station = tile_id - station_id*nTiles_phi*nTiles_z;

    return nTiles_z - (int)(tile_id_on_station / nTiles_phi) -1;
}

// create hits of an event on a specific tile;
// *tile_id  - ID of the tile which is hit
// *event    - pointer to the event where the hit comes from
// *rand     - random generator
// *f_noise  - wether noise is enabled or not
void create_hit(int tile_id, Event* event, TRandom3* rand, bool f_noise) {
    int iEvent = event->id;
    double time = event->time;
    Tile* tile = &vTiles[tile_id];

//    printf("Event: %d\n", iEvent);

    double timestamp = 0;
  
    //decide if noise event, or not
    if(rand->Uniform(100.0) <= 100*NOISE_RATE && f_noise) {
        //noise window: +-50ns
        timestamp = time + rand->Uniform(100.0) - 50;
    } else {
        timestamp = time + rand->Gaus(tile->offset,tile->jitter);
    }
//    printf("Event: %d\t in tile %d\t at time %3.3f,\t with offset %3.3f\t %3.3f\n", iEvent, tile_id, time, time - timestamp, tile->offset);
    event->tiles.push_back(tile_id);
    event->hits.push_back(timestamp);

    nHits++;
}

//Reads the file fTiles with a tree of tiles and pushes them into vTiles
void read_tiles(const char* fTiles) {
    TFile *file = new TFile(fTiles);

    TTree *tree = (TTree*)file->Get("tree");

    Tile *tile = new Tile();

    tree->SetBranchAddress("tile",&tile);
    
    printf("Read %d tiles of a total of %d tiles from %s.\n", nTiles, tree->GetEntries()/1, fTiles);
    printf("\n");

    for(int i=0; i<nTiles; i++) {
        tree->GetEntry(i);
        vTiles.push_back(*tile);
    }

    file->Close();
}

int main() {
    printf("Started generation of events ...\n");
    printf("\n");

    clock_t tStart = clock();


    TRandom3 *rand = new TRandom3(0);

//    const char* fEvName = "events/events.root";

//    printf("Save in file: \"%s\" \n", fEvName);
    printf("Save in file: \"%s\" \n", fDtName);
    printf("\n");

    read_tiles(fTilesName);

//    TTree *evTree = new TTree("tree", "A Tree with Events");

    Event* event = 0;   // create event dummy
//    evTree->Branch("event", "Event", &event); // set address of branch to the pointer of event

//    TFile ftmp("tmp.root","recreate");      //write to this file
    TTree *dtTree = new TTree("tree", "A Tree with Time Differences");

    Int_t ttile1 = 0;
    Int_t ttile2 = 0;
    Int_t tEvent = 0;
    Double_t tdT = 0;

    dtTree->Branch("tile1", &ttile1, "ttile1/I");
    dtTree->Branch("tile2", &ttile2, "ttile2/I");
    dtTree->Branch("event", &tEvent, "tEvent/I");
    dtTree->Branch("dT", &tdT, "tdT/D");
    

    int n_e_events = 0;
    int n_ic_events = 0;

//    int iEvent = 0;
    //create events
//    while(Time < MAX_TIME*1e9) {
    for(int iEvent=0; iEvent < nEvents; iEvent++) {
        Time += rand->Gaus(1.0/max(RATE_e,RATE_IC)*1e9, 10);

        event = new Event();

        event->id = iEvent;
        tEvent = iEvent;
        event->time = Time;

        if(RATE_e != 0 && rand->Uniform(1) > RATE_IC/RATE_e) { //single electron event; mu -> evv

            //central tile
            int tile_id = (int)rand->Uniform(nTiles);
            create_hit(tile_id, event, rand, f_noise);

            if(f_crosstalk) { //CT Hits {{{
                if(rand->Uniform(100) <= 50 && tile_dist_from_edge(tile_id) > 0) {
                    int tile_id_2 = tile_id + nTiles_phi;
                    if(rand->Uniform(100) <= 2*6 && tile_dist_from_edge(tile_id) > 1) {
                        int tile_id_2 = tile_id + 2*nTiles_phi;
                        create_hit(tile_id_2, event, rand, f_noise);
                    }
                }
                if(rand->Uniform(100) <= 15 && tile_dist_from_edge(tile_id+1-nTiles_phi) > 0) {
                    int tile_id_2 = tile_id + 1;
                    create_hit(tile_id_2, event, rand, f_noise);
                }
                if(rand->Uniform(100) <= 15 && tile_dist_from_edge(tile_id+1) > 0 && tile_id < nTiles-1) {
                    int tile_id_2 = tile_id + 1 + nTiles_phi;
                    create_hit(tile_id_2, event, rand, f_noise);
                }

                if(rand->Uniform(100) <= 7 && tile_id > 0) {
                    int tile_id_2 = tile_id - 1;
                    create_hit(tile_id_2, event, rand, f_noise);
                }
                if(rand->Uniform(100) <= 7 && tile_dist_from_edge(tile_id) > 0) {
                    int tile_id_2 = tile_id - 1 + nTiles_phi;
                    if(tile_id_2 > nTiles)
                    create_hit(tile_id_2, event, rand, f_noise);
                }
            }
            //}}}

            if(event->tiles.size() > 2) {
                n_e_events++;
            }
        } else { //mu -> eeevv event
             
            for(int i=0; i<3; i++) {
                int tile_id = (int)rand->Uniform(nTiles);
                create_hit(tile_id, event, rand, f_noise);
                if(f_crosstalk) { //CT Hits {{{
                    if(rand->Uniform(100) <= 50 && tile_dist_from_edge(tile_id) > 0) {
                        int tile_id_2 = tile_id + nTiles_phi;
                        if(rand->Uniform(100) <= 2*6 && tile_dist_from_edge(tile_id) > 1) {
                            int tile_id_2 = tile_id + 2*nTiles_phi;
                            create_hit(tile_id_2, event, rand, f_noise);
                        }
                    }
                    if(rand->Uniform(100) <= 15 && tile_dist_from_edge(tile_id+1-nTiles_phi) > 0) {
                        int tile_id_2 = tile_id + 1;
                        create_hit(tile_id_2, event, rand, f_noise);
                    }
                    if(rand->Uniform(100) <= 15 && tile_dist_from_edge(tile_id+1) > 0 && tile_id < nTiles-1) {
                        int tile_id_2 = tile_id + 1 + nTiles_phi;
                        create_hit(tile_id_2, event, rand, f_noise);
                    }

                    if(rand->Uniform(100) <= 7 && tile_id > 0) {
                        int tile_id_2 = tile_id - 1;
                        create_hit(tile_id_2, event, rand, f_noise);
                    }
                    if(rand->Uniform(100) <= 7 && tile_dist_from_edge(tile_id) > 0) {
                        int tile_id_2 = tile_id - 1 + nTiles_phi;
                        create_hit(tile_id_2, event, rand, f_noise);
                    }
                }
            } //}}}

            if(event->tiles.size() > 2) {
                n_ic_events++;
            }
        }

        // take just events with more than 1 hit
        if(event->tiles.size() > 2) {
            for(int i=0; i<event->tiles.size()-1; i++) {
                double time1 = event->hits[i];
                for(int j=i+1; j<event->tiles.size(); j++) {
                    ttile1 = event->tiles[i];
                    ttile2 = event->tiles[j];
                    if(ttile1 != ttile2) {
                        //ttile1 should be smaller value;
                        if(ttile1 > ttile2) {
                            int tmptile = ttile1;
                            ttile1 = ttile2;
                            ttile2 = tmptile;
                            tdT = -1.0*(time1 - event->hits[j]);
                        } else {
                            tdT = time1 - event->hits[j];
                        }
//                        printf("%d,%d:\ttile1: %d,\t tile2: %d,\t dT: %3.3f\t from Event %d\n", i,j, ttile1, ttile2, tdT, tEvent);
                        dtTree->Fill();
                    }
                }


            }
        } else {
            iEvent--;
            nHits-= event->tiles.size();
        }
//        printf("\n --------------\n");
        delete event;

        if(iEvent%100000 == 0) {
            printf("%d\n", iEvent);
        }
    }

//    dtTree->Write();
//    ftmp.Close();



    printf("Generated %d events\n", nEvents);
    printf("%d of them are internal conversion, and %d are single e.\n", n_ic_events, n_e_events);
//    printf("->with a total of %d hits. Tree says: %d\n", nHits,dtTree->GetEntries()/1);
    printf("->with a total of %d hits. \n", nHits);
    printf("\n");

    //Save Tree
//    TFile f(fEvName,"recreate");      //write to this file
//    evTree->Write();
//    f.Close();

//    TFile *file = new TFile("tmp.root");
//
//    TTree *dt2Tree = (TTree*)file->Get("tree");

    //sort tree
    Int_t t_tile1 = 0;
    Int_t t_tile2 = 0;
    Int_t t_event = 0;
    Double_t t_dT = 0;

    dtTree->SetBranchAddress("tile1", &t_tile1);
    dtTree->SetBranchAddress("tile2", &t_tile2);
    dtTree->SetBranchAddress("event", &t_event);
    dtTree->SetBranchAddress("dT", &t_dT);


    TFile fdT(fDtName,"recreate");      //write to this file
    TTree *sort_dtTree = new TTree("tree", "A Tree with Time Differences - Sorted");

    Int_t st_tile1 = 0;
    Int_t st_tile2 = 0;
    Double_t st_dT = 0;

    sort_dtTree->Branch("tile1", &st_tile1, "st_tile1/I");
    sort_dtTree->Branch("tile2", &st_tile2, "st_tile2/I");
    sort_dtTree->Branch("dT", &st_dT, "st_dT/D");

    dtTree->BuildIndex("tile1","tile2");
    TTreeIndex *index = (TTreeIndex*)dtTree->GetTreeIndex();

    for( int i = index->GetN() - 1; i >=0 ; --i ) {
        Long64_t local = dtTree->LoadTree( index->GetIndex()[i] );
//        printf("After LoadTree: %3.6fs\n",(clock()-tStart)/(double)CLOCKS_PER_SEC);
        dtTree->GetEntry(local);
//        printf("After getEntry: %3.6fs\n",(clock()-tStart)/(double)CLOCKS_PER_SEC);
//        printf("local: %d\n", local);
        st_tile1 = t_tile1;
        st_tile2 = t_tile2;
        st_dT = (double)t_dT;
        if(i%1 == 1000000 || false) {
            printf("%d:\ttile1: %d,\t tile2: %d,\t dT: %3.3f\t from Event %d\n", i, st_tile1, st_tile2, t_dT, t_event);
        }

        sort_dtTree->Fill();
//        printf("\n");
    } 

    printf("%d\n",index->GetN());

    sort_dtTree->Write();
    fdT.Close();

    clock_t tEnd = clock();

    printf("Took: %3.3fs\n",(tEnd-tStart)/(double)CLOCKS_PER_SEC);

//    file->Close();
//    ftmp.Close();
    fdT.Close();

    return 0;
}
