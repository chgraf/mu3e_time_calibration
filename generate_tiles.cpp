#include "TRandom3.h"
#include "TFile.h"
#include "TTree.h"
#include "TObject.h"

#include "EventType.h"

using namespace std;

const double MEAN_JITTER = 0.1; //[ns]

const int nTiles_phi = 1;  //56
const int nTiles_z = 10;    //60
const int nStations = 1;    //4

int nTiles = nTiles_phi * nTiles_z * nStations;

//edge tile has distance 0
int tile_dist_from_edge(int tile_id) { 
    int station_id = tile_id / (nTiles_phi*nTiles_z); // / rounds down -> station_id of tile;
    int tile_id_on_station = tile_id - station_id*nTiles_phi*nTiles_z;

    return nTiles_z - (int)(tile_id_on_station / nTiles_phi) -1;
}

int main() {
    printf("Started generation of tiles ...\n");
    printf("\n");
    printf("Generate with %d tiles in phi, %d tiles in z and %d stations.\n", nTiles_phi, nTiles_z, nStations);
    printf("This makes a total of %d tiles.\n", nTiles);
    printf("With %d tiles per station.\n", nTiles_phi * nTiles_z);
    printf("\n");
    TRandom3 *rand = new TRandom3(0);

    const char* fname = "tiles/tiles10.root";

    TFile f(fname,"recreate");      //write to this file
    TTree *tree = new TTree("tree", "A Tree with Tiles");
    
    printf("-> Save in file: \"%s\" \n", fname);
    printf("\n");

    Tile* tile = 0;   // create event dummy

    tree->Branch("tile", "Tile", &tile); // set address of branch to the pointer of event


    for(int iTile=0; iTile < nTiles; iTile++) {
        tile = new Tile();

        tile->id = iTile;
        tile->offset = rand->Uniform(100);
        tile->jitter = rand->Gaus(MEAN_JITTER,0.02);

        tree->Fill();

        if(iTile < 5 || iTile > nTiles-6) {
            printf("initialize iTile %d: offset: %.3f ns \t jitter: %.3f ns \t dist end of station: %d \n", iTile, tile->offset, tile->jitter, tile_dist_from_edge(iTile));
        }
        if(iTile == 5) {
            printf("... \n");
            printf("... \n");
            printf("... \n");
        }

        delete tile;
    }


    //Clean Up
    tree->Write();
    f.Close();

    return 0;
}
