ROOTCFLAGS   := $(shell root-config --cflags)
ROOTLIBS     := $(shell root-config --libs)
ROOTGLIBS    := $(shell root-config --glibs)

LIBS         := dict.o

LDFLAGS      :="-Wl,--no-as-needed"

CC = g++

all: dict.cpp dict.o simulation generate_events generate_tiles analyse_events

dict.cpp: EventType.h LinkDef.h
	@echo "Generating Dictionary $@..."
	@rootcint -f $@ -c $^ 

dict.o: dict.cpp dict.h
	@echo "Compiling $< ..."
	@$(CC) -fPIC $(INC) $(ROOTCFLAGS) $(ROOTGLIBS) -c dict.cpp

simulation: simulation.cpp 
	@echo "Compiling $< ..."
	@$(CC) $(LDFLAGS) $(ROOTCFLAGS) $(ROOTLIBS) $(ROOTGLIBS) $^ -o $@

generate_events: generate_events.cpp
	@echo "Compiling $< ..."
	@$(CC) $(LDFLAGS) $(ROOTCFLAGS) $(ROOTLIBS) $(ROOTGLIBS) $(LIBS) $^ -o $@

generate_tiles: generate_tiles.cpp
	@echo "Compiling $< ..."
	@$(CC) $(LDFLAGS) $(ROOTCFLAGS) $(ROOTLIBS) $(ROOTGLIBS) $(LIBS) $^ -o $@

analyse_events: analyse_events.cpp
	@echo "Compiling $< ..."
	@$(CC) $(LDFLAGS) $(ROOTCFLAGS) $(ROOTLIBS) $(ROOTGLIBS) $(LIBS) $^ -o $@
