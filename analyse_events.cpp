#include <stdio.h>
#include <stdlib.h> //atof
#include <math.h> //fabs
#include <time.h> //clock

#include <algorithm> //std::sort
#include <sstream> //ss

#include "TFile.h"
#include "TTree.h"
#include "TSQLResult.h"
#include "TSQLRow.h"

#include "EventType.h"

using namespace std;

const bool f_noise_red = false;
const double MEAN_JITTER = 0.1;

int nTiles = 0;
vector<Tile> vTiles;

void read_tiles(const char* fTiles) {
    TFile *file = new TFile(fTiles);

    TTree *tree = (TTree*)file->Get("tree");

    Tile *tile = new Tile();

    tree->SetBranchAddress("tile",&tile);
    
    nTiles = tree->GetEntries()/1;

    printf("Read %d tiles of a total of %d tiles from %s.\n", nTiles, tree->GetEntries()/1, fTiles);
    printf("\n");


    for(int i=0; i<nTiles; i++) {
        tree->GetEntry(i);
        vTiles.push_back(*tile);
    }

    file->Close();
}

//Takes the matrix from generate events and averages all relative timing information
void average_relative_timing(const char* fdT) {
    printf("building relative timing matrix ... \n");

    //input tree
    TFile *file = new TFile(fdT);
    TTree *iTree = (TTree*)file->Get("tree");

    Int_t ttile1 = 0;
    Int_t ttile2 = 0;
    Double_t tdT = 0;

    iTree->SetBranchAddress("tile1", &ttile1);
    iTree->SetBranchAddress("tile2", &ttile2);
    iTree->SetBranchAddress("dT", &tdT);

    int nEntries = iTree->GetEntries()/1;

    printf("Read a total of %d timing information from %s.\n", nEntries, fdT);
    printf("\n");

    //output tree
    TFile rfile("rel_t.root", "recreate");
    TTree *oTree = new TTree("tree", "A Tree with All Results");

    Int_t rt_tile1 = 0;
    Int_t rt_tile2 = 0;
    Double_t rt_dT = 0;
    Int_t rt_n = 0;

    oTree->Branch("tile1", &rt_tile1, "rt_tile1/I");
    oTree->Branch("tile2", &rt_tile2, "rt_tile2/I");
    oTree->Branch("dT", &rt_dT, "rt_dT/D");
    oTree->Branch("N", &rt_n, "rt_n/I");

    clock_t tStart = clock();

    //initialize empty vector
    vector<double> vEmpty;
    vector<vector<double> > mdT;    //matrix odouble averaged dTs
    for(int iTile=0; iTile<nTiles; iTile++) {
        vEmpty.push_back(0);
    }
    for(int iTile=0; iTile<nTiles; iTile++) {
        mdT.push_back(vEmpty);
    }

    vector<double> vdT;
    double dT = 0;
    int c_tile1 = 0;
    int c_tile2 = 0;

    int nThrow_away = 0;

    for(int i=0; i<nEntries; i++) {

        iTree->GetEntry(i);

        if((ttile1 != c_tile1 || ttile2 != c_tile2) && vdT.size() > 0 || i == nEntries-1) {
            sort(vdT.begin(), vdT.end());

            double median = vdT[0];
            if(vdT.size()/1 > 1) {
                median = vdT[(int)(vdT.size()/2)];
            }

            double mean_dT = 0;
            int nValid_dTs = 0;

            for(int k=0; k<vdT.size(); k++) {
                if(fabs(vdT[k] - median) < MEAN_JITTER*3 || not f_noise_red) {
                    mean_dT += vdT[k];
                    if(c_tile1+c_tile2 == 100 || true) {
                        printf("%3.3f, ",vdT[k]);
                    }

                    nValid_dTs++;
                } else {
                    nThrow_away++;
                }
            }

//            printf("%d, %d\n", c_tile2, ttile2);
            mdT[c_tile1][c_tile2] = (double)(mean_dT/(double)nValid_dTs);
            rt_tile1 = c_tile1;
            rt_tile2 = c_tile2;
            rt_dT = mdT[c_tile1][c_tile2];
            rt_n = nValid_dTs;
            oTree->Fill();

            if(c_tile1+c_tile2 == 100 || true) {
                printf("\n");
                printf("tile1: %d,\t tile2: %d,\t dT: %3.3f\n",c_tile1, c_tile2, mdT[c_tile1][c_tile2]);
                printf("n: %d\n",nValid_dTs);
                printf("Truth: %3.3f\n", vTiles[c_tile1].offset - vTiles[c_tile2].offset);
                printf("\n");
            }

            vdT.clear();
            c_tile1 = ttile1;
            c_tile2 = ttile2;
//            printf("clear\n");
        }

        vdT.push_back(tdT);
    }

    printf("Thrown away events due to noise reduction: %d\n", nThrow_away);

    file->Close();

    oTree->Write();
    rfile.Close();

    printf("Build Relative Timing Matrix in %3.6fs\n",(clock()-tStart)/(double)CLOCKS_PER_SEC);
    printf("\n");
}

//Build vector of relative timing from matrix of relative timing
void estimate_m1() {
    printf("Start estimation ...\n");

    //input tree
    TFile *file = new TFile("rel_t.root");

    TTree *iTree = (TTree*)file->Get("tree");

    Int_t ttile1 = 0;
    Int_t ttile2 = 0;
    Double_t tdT = 0;
    Int_t tN = 0;

    iTree->SetBranchAddress("tile1", &ttile1);
    iTree->SetBranchAddress("tile2", &ttile2);
    iTree->SetBranchAddress("dT", &tdT);
    iTree->SetBranchAddress("N", &tN);

    int nEntries = iTree->GetEntries()/1;

    printf("Read a total of %d relative timing information from rel_t.root\n", nEntries);
    printf("\n");

    //output tree
    TFile rfile("result.root", "recreate");
    TTree *res_tree = new TTree("tree", "A Tree with All Results");

    Int_t rt_tile = 0;
    Double_t rt_dT = 0;
    Int_t rt_n = 0;
    Double_t rt_truth = 0;
    Double_t rt_error = 0;

    res_tree->Branch("tile", &rt_tile, "rt_tile/I");
    res_tree->Branch("estimation", &rt_dT, "rt_dT/D");
    res_tree->Branch("N", &rt_n, "rt_n/I");
    res_tree->Branch("truth", &rt_truth, "rt_truth/D");
    res_tree->Branch("error", &rt_error, "rt_error/D");


    //initialize empty vector
    vector<double> vEmpty;
    vector<vector<double> > mdT;    //matrix of averaged dTs
    vector<vector<double> > mStdev;    //matrix of Stdev
    vector<vector<double> > mNdT;    //matrix of number of entries
    for(int iTile=0; iTile<nTiles; iTile++) {
        vEmpty.push_back(0);
    }
    for(int iTile=0; iTile<nTiles; iTile++) {
        mdT.push_back(vEmpty);
        mStdev.push_back(vEmpty);
        mNdT.push_back(vEmpty);
    }

    //Get dTs from Tree
    for(int i=0; i<nEntries; i++) {
        iTree->GetEntry(i);
        mdT[ttile1][ttile2] = tdT;
        mNdT[ttile1][ttile2] = tN;
//        printf("\n");
//        printf("tile1: %d,\t tile2: %d,\t dT: %3.3f\n",ttile1, ttile2, mdT[ttile1][ttile2]);
//        printf("n: %d\n",tN);
//        printf("Truth: %3.3f\n", vTiles[ttile1].offset - vTiles[ttile2].offset);
//        printf("\n");
     }

    double stdev = 0;
    for(int i=1; i < nTiles; i++) {
        double dt = 0;
        if(mNdT[0][i] > 0) {
            stdev = MEAN_JITTER / (double)sqrt((double)mNdT[0][1]);
            stdev = 1;
            dt = mdT[0][i]*stdev;
        } else {
            stdev = 0;
        }
        double stdev_sum = stdev;
        int count = 1;

        printf("For tile 0 to %d\n",i);

        //Average over matrix elements
        for(int j=1; j < nTiles; j++) {
            if(i < j) {
                if(mNdT[0][j] > 0 && mNdT[i][j] > 0) {
                    printf("%3.3f = [0,%d] - [%d,%d], ", mdT[0][j] - mdT[i][j], j, i, j);
                    stdev = MEAN_JITTER * sqrt(pow(1 / (double)sqrt((double)mNdT[0][j]), 2) + pow(1 / (double)sqrt((double)mNdT[i][j]), 2) );
                    stdev = 1;
                    dt += (mdT[0][j] - mdT[i][j])*stdev;
                    stdev_sum += stdev;
                    count++;
                }
            } else {
                if(mNdT[0][j] > 0 && mNdT[j][i] > 0) {
                    printf("%3.3f = [0,%d] + [%d,%d], ", mdT[0][j] + mdT[j][i], j, j, i);
                    stdev = MEAN_JITTER * sqrt(pow(1 / (double)sqrt((double)mNdT[0][j]), 2) + pow(1 / (double)sqrt((double)mNdT[j][i]), 2) );
                    stdev = 1;
                    dt += (mdT[0][j] + mdT[j][i])*stdev;
                    stdev_sum += stdev;
                    count++;
                }
            }
        }
        
        printf("\n->%3.3f\n", mdT[0][i]);
        
        double estimation = dt / (double)stdev_sum;
        
        rt_tile = i;
        rt_dT = estimation;
        rt_n = count;
        rt_truth = vTiles[0].offset - vTiles[i].offset;
        rt_error = estimation - rt_truth;


        printf("dt: %3.3f\tN: %d\n", dt, count);
        printf("Est: %3.3f\ttruth: %3.3f\tError: %3.3f\n", estimation, rt_truth, estimation-rt_truth);
        printf("\n");

        res_tree->Fill();
    }

    res_tree->Write();

}

int main() {
    printf("Started analysis of events ... \n");

    read_tiles("tiles/tiles10.root");

    average_relative_timing("dTs/dT1e4-10T-n-c.root");

    estimate_m1();

    return 0;
}
