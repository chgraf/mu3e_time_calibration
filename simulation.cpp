// Komplexität:
// Event*(Hits per Events)^2 + Tile^2
// x 1e6 Events, 10 Tiles -> ~2s
// x 1e6 Events, 10000 Tiles -> ~3s
// x 1e3 Events, 10000 Tiles -> ~2s
// -> 1e6 Events, 10000 Tiles -> ~45s
#include <iostream>
#include <algorithm> //for std::sort
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "TRint.h"
#include "TRandom3.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraph.h"
#include "TMultiGraph.h"

using namespace std;


const int nTiles_phi = 56;  //56
const int nTiles_z = 60;    //60
const int nStations = 2;    //4

// 0: compare with one master tile
// 1: every tile is master tile
const int METHOD = 1;
const bool NOISE_RED = true; //
const bool CROSSTALK = true;

const int nTiles = nTiles_phi*nTiles_z*nStations;
//    const int nTiles = 56*60*4;
const double MAX_TIME = 5000; //[s]
const double MIN_TIME = 1; //[s]
const double TIME_STEPS = 5; //[s]

//TODO: Check Rates!
const double RATE_IC = 1e3; // 1e3[Hz]
const double RATE_e = 0e5;//1e1; // 10e6[Hz]
const double CALIBRATION_THR = 0.02; //[ns]
const double MEAN_JITTER = 0.1; //[ns]

const double NOISE_RATE = 0.05; //Noise rate

TH1D *h_timestamps = new TH1D("h_timestamps","Hit Timestamps",1000000,0,1000000);
TH1D *h_hits = new TH1D("h_hits","Hit in Tiles",nTiles,0,nTiles);
//TH1D *h_events_per_tile = new TH1D("h_events_per_tile","Events per Tile",50,2500,3500);

TH1D *h_error = new TH1D("h_error","Estimation Error",2000,-1,1);
TH1D *h_debug = new TH1D("h_debug","DEBUG",20000,-100,100);
TH1D *h_debug2 = new TH1D("h_debug2","DEBUG",2000,-1,1);
TH2D *h2_debug = new TH2D("h2_debug","DEBUG",200,-100,100, 10000, -1, 1);

TH2D *h2_rel_timing = new TH2D("h2_rel_timing","Relative Timing",nTiles,0,nTiles, nTiles, 0, nTiles);
TH2D *h2_statistics = new TH2D("h2_statistics","Number of Events for Relative Timing",nTiles,0,nTiles, nTiles, 0, nTiles);

TH2D *h2_timing_error = new TH2D("h2_statistics","Timing",nTiles_z*nStations,0,nTiles_z*nStations, nTiles_phi, 0, nTiles_phi);
TH2D *h2_calibrated = new TH2D("h2_calibrated","Calibrated Tiles",nTiles_z*nStations,0,nTiles_z*nStations, nTiles_phi, 0, nTiles_phi);

struct Event {
    double id;
    double time;
    vector<int> tiles;
    vector<double> hits;
};

struct Tile {
    double id;
    double offset;
    double jitter;
//    vector<int> events;
};


vector<Tile> vTiles; //stores the time of the hits for each tile
vector<Event> vEvents;


//edge tile has distance 0
int tile_dist_from_edge(int tile_id) { 
    int station_id = tile_id / (nTiles_phi*nTiles_z); // / rounds down -> station_id of tile;
    int tile_id_on_station = tile_id - station_id*nTiles_phi*nTiles_z;

    return nTiles_z - (int)(tile_id_on_station / nTiles_phi) -1;
}

void create_hit(int tile_id, Event* event, double time, TRandom3* rand, bool f_noise) {
    int iEvent = event->id;
    Tile* tile = &vTiles[tile_id];

//    printf("Event: %d\n", iEvent);


    double timestamp = 0;
    //decide if noise event, or not
    if(rand->Uniform(100.0) <= 100*NOISE_RATE && f_noise) {
        //noise window: 50ns
        timestamp = time + rand->Uniform(100.0) - 50;
    } else {
        timestamp = time + rand->Gaus(tile->offset,tile->jitter);
    }
    event->tiles.push_back(tile_id);
    event->hits.push_back(timestamp);

    h_timestamps->Fill(timestamp);
    h_hits->Fill(tile_id);
}

double Time = 0; // in ns

int n_e_events = 0;
int n_ic_events = 0;
int nEvents = 0;

vector<double> vAnalyse_time;
vector<double> vnCalibrated_m0;
vector<double> vnCalibrated_m1;
vector<double> vMean_error_m0;
vector<double> vMean_error_m1;

vector<double> vnCalibrated_ct;
vector<double> vMean_error_ct;

vector<double> vnCalibrated_noise;
vector<double> vMean_error_noise;

vector<double> vnCalibrated_noise_red;
vector<double> vMean_error_noise_red;

void reset() {
    Time = 0;
    n_e_events = 0;
    n_ic_events = 0;
    nEvents = 0;
    vTiles.clear();
    vEvents.clear();
}

//
// ------>> -------- <<------ //
// ------>> Simulate <<------ //
// ------>> -------- <<------ //
void simulate(TRandom3 *rand, bool f_crosstalk, bool f_noise) { //{{{
    //initialize iTiles, everything in ns
    for(int iTile=0; iTile < nTiles; iTile++) {
        Tile tile;
        tile.id = iTile;
        tile.offset = rand->Uniform(100);
        tile.jitter = rand->Gaus(MEAN_JITTER,0.02);
        if(iTile < 5 || iTile > nTiles-6) {
            printf("initialize iTile %d: offset: %.3f \t jitter: %.3f \t dist end of station: %d \n", iTile, tile.offset, tile.jitter, tile_dist_from_edge(iTile));
        }
        if(iTile == 5) {
            printf("... \n");
            printf("... \n");
            printf("... \n");
        }
        vTiles.push_back(tile);
    }

    int iEvent = 0;
    //create events
    while(Time < MAX_TIME*1e9) {
        Time += rand->Gaus(1.0/max(RATE_e,RATE_IC)*1e9, 10);

        Event event;
        event.id = iEvent;
        event.time = Time;

        if(RATE_e != 0 && rand->Uniform(1) > RATE_IC/RATE_e) { //single electron event; mu -> evv

            //central tile
            int tile_id = (int)rand->Uniform(nTiles);
            create_hit(tile_id, &event, Time, rand, f_noise);

            if(f_crosstalk) { //CT Hits {{{
                if(rand->Uniform(100) <= 50 && tile_dist_from_edge(tile_id) > 0) {
                    int tile_id_2 = tile_id + nTiles_phi;
                    if(tile_id_2 > nTiles)
                        printf("1 -> tile_id_2: %d\n" ,tile_id_2);
                    if(rand->Uniform(100) <= 2*6 && tile_dist_from_edge(tile_id) > 1) {
                        int tile_id_2 = tile_id + 2*nTiles_phi;
                        if(tile_id_2 > nTiles)
                            printf("2 -> tile_id_2: %d\n" ,tile_id_2);
                        create_hit(tile_id_2, &event, Time, rand, f_noise);
                    }
                }
                if(rand->Uniform(100) <= 15 && tile_dist_from_edge(tile_id+1-nTiles_phi) > 0) {
                    int tile_id_2 = tile_id + 1;
                    if(tile_id_2 > nTiles)
                        printf("3 -> tile_id_2: %d\n" ,tile_id_2);
                    create_hit(tile_id_2, &event, Time, rand, f_noise);
                }
                if(rand->Uniform(100) <= 15 && tile_dist_from_edge(tile_id+1) > 0 && tile_id < nTiles-1) {
                    int tile_id_2 = tile_id + 1 + nTiles_phi;
                    if(tile_id_2 > nTiles)
                        printf("4 -> tile_id_2: %d\n" ,tile_id_2);
                    create_hit(tile_id_2, &event, Time, rand, f_noise);
                }

                if(rand->Uniform(100) <= 7 && tile_id > 0) {
                    int tile_id_2 = tile_id - 1;
                    if(tile_id_2 > nTiles)
                        printf("5 -> tile_id_2: %d\n" ,tile_id_2);
                    create_hit(tile_id_2, &event, Time, rand, f_noise);
                }
                if(rand->Uniform(100) <= 7 && tile_dist_from_edge(tile_id) > 0) {
                    int tile_id_2 = tile_id - 1 + nTiles_phi;
                    if(tile_id_2 > nTiles)
                        printf("6 -> tile_id_2: %d\n" ,tile_id_2);
                    create_hit(tile_id_2, &event, Time, rand, f_noise);
                }
            }
            //}}}

            n_e_events++;
        } else { //mu -> eeevv event
             
            for(int i=0; i<3; i++) {
                int tile_id = (int)rand->Uniform(nTiles);
                create_hit(tile_id, &event, Time, rand, f_noise);
                if(f_crosstalk) { //CT Hits {{{
                    if(rand->Uniform(100) <= 50 && tile_dist_from_edge(tile_id) > 0) {
                        int tile_id_2 = tile_id + nTiles_phi;
                        if(tile_id_2 > nTiles)
                            printf("1 -> tile_id_2: %d\n" ,tile_id_2);
                        if(rand->Uniform(100) <= 2*6 && tile_dist_from_edge(tile_id) > 1) {
                            int tile_id_2 = tile_id + 2*nTiles_phi;
                            if(tile_id_2 > nTiles)
                                printf("2 -> tile_id_2: %d\n" ,tile_id_2);
                            create_hit(tile_id_2, &event, Time, rand, f_noise);
                        }
                    }
                    if(rand->Uniform(100) <= 15 && tile_dist_from_edge(tile_id+1-nTiles_phi) > 0) {
                        int tile_id_2 = tile_id + 1;
                        if(tile_id_2 > nTiles)
                            printf("3 -> tile_id_2: %d\n" ,tile_id_2);
                        create_hit(tile_id_2, &event, Time, rand, f_noise);
                    }
                    if(rand->Uniform(100) <= 15 && tile_dist_from_edge(tile_id+1) > 0 && tile_id < nTiles-1) {
                        int tile_id_2 = tile_id + 1 + nTiles_phi;
                        if(tile_id_2 > nTiles)
                            printf("4 -> tile_id_2: %d\n" ,tile_id_2);
                        create_hit(tile_id_2, &event, Time, rand, f_noise);
                    }

                    if(rand->Uniform(100) <= 7 && tile_id > 0) {
                        int tile_id_2 = tile_id - 1;
                        if(tile_id_2 > nTiles)
                            printf("5 -> tile_id_2: %d\n" ,tile_id_2);
                        create_hit(tile_id_2, &event, Time, rand, f_noise);
                    }
                    if(rand->Uniform(100) <= 7 && tile_dist_from_edge(tile_id) > 0) {
                        int tile_id_2 = tile_id - 1 + nTiles_phi;
                        if(tile_id_2 > nTiles)
                            printf("6 -> tile_id_2: %d\n" ,tile_id_2);
                        create_hit(tile_id_2, &event, Time, rand, f_noise);
                    }
                }
            } //}}}
            n_ic_events++;
        }

        // take just events with more than 1 hit
        if(event.tiles.size() > 2) {
            vEvents.push_back(event);
            iEvent++;
        }
    }

    nEvents = iEvent;


    printf("Generated %d events\n", nEvents);
}

// ------>> ------- <<------ //
// ------>> Analyse <<------ //
// ------>> ------- <<------ //
void analyse(TRandom3 *rand, double analyse_time, bool f_crosstalk, bool f_noise_red, bool f_noise) {
    vector<double> vEst_offset_m0;
    vector<int> vnEst_m0;
    vector<double> vEst_stdev_m0;
    vector<double> vEst_offset_m1;
    vector<int> vnEst_m1;
    vector<double> vEst_stdev_m1;
    vector<vector<double> > mEst_offset;
    vector<vector<vector<double> > > mvPre_est_offset;
    vector<vector<double> > mEst_stdev;
    vector<vector<int> > mnEst;          //number of calibration events

    vector<double> vEmpty;
    vector<vector<double> > mEmpty;

    vector<vector<double> > mDt;
    vector<vector<double> > mDt_stdev;
    
    int count_throw_away = 0;

    //initialize vectors and matrices
    for(int jTile=0; jTile < nTiles; jTile++) {
        vEst_offset_m0.push_back(0);
        vEst_stdev_m0.push_back(0);
        vnEst_m0.push_back(0);
        vEst_offset_m1.push_back(0);
        vEst_stdev_m1.push_back(0);
        vnEst_m1.push_back(0);

        mEmpty.push_back(vEmpty);
    }
    for(int iTile=0; iTile < nTiles; iTile++) {
        mEst_offset.push_back(vEst_offset_m0);
        mEst_stdev.push_back(vEst_offset_m0);
        mnEst.push_back(vnEst_m0);

        mDt.push_back(vEmpty);
        mDt_stdev.push_back(vEmpty);

        mvPre_est_offset.push_back(mEmpty);

//        h_events_per_tile->Fill(vTiles[iTile].events.size());
    }

    int nCalibration_event_m0 = 0;
    int nCalibration_event_m1 = 0;
    
    //loop through all events to build pre-estimation matrix of timing vectors
    for(int iEvent=0; iEvent < vEvents.size(); iEvent++)  {
//        printf("\n Event %d",iEvent);
        Event event = vEvents[iEvent];
        
        // just consider events in a certain time range
        if(event.time < analyse_time*1e9) {
            for(int i=0; i<event.tiles.size(); i++) {
    //            printf(" %d ", i);
                for(int k=i+1; k<event.tiles.size(); k++) { int tile1 = event.tiles[i];
                    int tile2 = event.tiles[k];
    //                printf(" [%d]->(%d, %d)", k, tile1, tile2);
                    if(tile1 != tile2) {
                        if(tile1 == 0 || tile2 == 0) {
                            nCalibration_event_m0++;
                        }
                        nCalibration_event_m1++;
                        //swap tiles, if first is bigger;
                        if(tile1 > tile2) {
                            mvPre_est_offset[tile2][tile1].push_back((double)(event.hits[i] - event.hits[k]));
    //                        mEst_offset[tile2][tile1] += (double)(event.hits[i] - event.hits[k]);
    //                        mnEst[tile2][tile1]++;
                        } else {
                            mvPre_est_offset[tile1][tile2].push_back((double)(event.hits[k] - event.hits[i]));
    //                        mEst_offset[tile1][tile2] += (double)(event.hits[k] - event.hits[i]);
    //                        mnEst[tile1][tile2]++;
                        }

                    }
                }
            }
        }
    }

    printf("pre-estimation matrix build\n");
    
    //build estimation matrix
    int count_zero = 0;
    for(int iTile=0; iTile < nTiles; iTile++) {
        for(int jTile=iTile; jTile < nTiles; jTile++) {
            int nEvents_pair = mvPre_est_offset[iTile][jTile].size();
//            printf("nEvents_pair: %d \n", nEvents_pair);
            if(nEvents_pair == 1) {
                mEst_offset[iTile][jTile] = mvPre_est_offset[iTile][jTile][0];
                mnEst[iTile][jTile]++;
            } else if(nEvents_pair == 2) { //if 2 events for this pair of tiles, use both
                mEst_offset[iTile][jTile] = mvPre_est_offset[iTile][jTile][0];
                mEst_offset[iTile][jTile] += mvPre_est_offset[iTile][jTile][1];
                mnEst[iTile][jTile]++;
                mnEst[iTile][jTile]++;
            } else if(nEvents_pair >= 3) { //throw away everything that is six sigma from median
                sort(mvPre_est_offset[iTile][jTile].begin(), mvPre_est_offset[iTile][jTile].end());
                double median = mvPre_est_offset[iTile][jTile][(int)(nEvents_pair/2)];
                for(int l=0; l<nEvents_pair; l++) {
                    if(fabs(mvPre_est_offset[iTile][jTile][l] - median) < MEAN_JITTER*6 || not f_noise_red) {
                        mEst_offset[iTile][jTile] += mvPre_est_offset[iTile][jTile][l];
                        mnEst[iTile][jTile]++;
                    } else {
                        count_throw_away++;
                    }
                }
            }


            if(mnEst[iTile][jTile] > 0) {
                mEst_offset[iTile][jTile] = mEst_offset[iTile][jTile] / (double)mnEst[iTile][jTile];
//                if(iTile==0) {
//                    double truth = vTiles[jTile].offset - vTiles[iTile].offset;
//                    h2_debug->Fill(truth, mEst_offset[iTile][jTile]-truth);
//                }
                //TODO: STDEV besser machen
                mEst_stdev[iTile][jTile] = MEAN_JITTER / (double)sqrt(mnEst[iTile][jTile]);
            } else count_zero++;

            h2_statistics->SetBinContent(iTile, jTile, mnEst[iTile][jTile]);
            if(mnEst[iTile][jTile] > 0) {
                h2_rel_timing->SetBinContent(iTile, jTile, fabs(mEst_offset[iTile][jTile] - (vTiles[jTile].offset - vTiles[iTile].offset)));
            } 
        }
    }

    //delete vector matrix
    mvPre_est_offset.clear();

    //METHOD 0
    vEst_offset_m0 = mEst_offset[0];

    //{{{
    if(nTiles <= 10) {
        printf("\n");
        printf("--- mEst_offset ---\n");
        for(int i=0; i<nTiles; i++) {
            for(int j=0; j<nTiles; j++) {
                printf("%3.3f\t", mEst_offset[i][j]);
            }
            printf("\n");
        }

        printf("\n");
        printf("--- mEst_stdev ---\n");
        for(int i=0; i<nTiles; i++) {
            for(int j=0; j<nTiles; j++) {
                printf("%3.3f\t", mEst_stdev[i][j]);
            }
            printf("\n");
        }

        printf("\n");
        printf("--- mnEst ---\n");
        for(int i=0; i<nTiles; i++) {
            for(int j=0; j<nTiles; j++) {
                printf("%d\t", mnEst[i][j]);
            }
            printf("\n");
        }

        printf("\n");
    }
    //}}}


    //METHOD 1
    for(int i=0; i<nTiles; i++) {
        double dt = 0;
        double error_sum = 0;
        double error_sq_sum = 0;
        int statistics = 0;

        //TODO: Check this for outliers
        if(mnEst[0][i] != 0) {
            mDt[i].push_back(mEst_offset[0][i]);
            mDt_stdev[i].push_back(mEst_stdev[0][i]);
        }

//        printf("%d:\t", i);
        for(int j=0; j<nTiles; j++) {
            if(i != j) {
                double stdev = 0;
                if(i < j) {
                    if(mnEst[0][j] > 0 && mnEst[i][j] > 0) {
//                        statistics += mnEst[0][j] + mnEst[i][j];
                        stdev = sqrt(pow(mEst_stdev[0][j],2) + pow(mEst_stdev[i][j],2));
                        mDt[i].push_back((mEst_offset[0][j] - mEst_offset[i][j]));
                        mDt_stdev[i].push_back(stdev);
//                        printf("%3.2f, ",mDt[i][j]);
//                        dt += (mEst_offset[0][j] - mEst_offset[i][j]) * stdev;
//                        double truth = vTiles[i].offset - vTiles[0].offset;

//                        h2_debug->Fill(truth, mDt[i].back()-truth);
                    }
                } else {
                    if(mnEst[0][j] > 0 && mnEst[j][i] > 0) {
//                        statistics += mnEst[0][j] + mnEst[j][i];
                        stdev = sqrt(pow(mEst_stdev[0][j],2) + pow(mEst_stdev[j][i],2));
                        mDt[i].push_back((mEst_offset[0][j] + mEst_offset[j][i]));
                        mDt_stdev[i].push_back(stdev);
//                        printf("%3.2f, ",mDt[i][j]);
//                        dt += (mEst_offset[0][j] + mEst_offset[j][i]) * stdev;
                    }
                }
            }
        }

//        printf("\n%d:\t", i);
        if(mDt[i].size() < 4 && f_noise_red) { //TODO: Was macht man hiermit?
            vnEst_m1[i] = 0;
            vEst_offset_m1[i] = 0;
            vEst_stdev_m1[i] = 0;
            count_throw_away += mDt[i].size();
        } else {
            sort(mDt[i].begin(), mDt[i].end());

            double median = 0;
            if (mDt[i].size() > 0) {
                median = mDt[i][(int)(mDt[i].size()/2)];
            }

            for(int k=0; k<mDt[i].size(); k++) {
                if(fabs(mDt[i][k] - median) < MEAN_JITTER*6 || not f_noise_red) {
                    dt += mDt[i][k]*mDt_stdev[i][k];
                    statistics++;
                    error_sum += mDt_stdev[i][k];
                    error_sq_sum += pow(mDt_stdev[i][k],2);
//                    printf("%3.2f, ",mDt[i][k]);
                    
                } else {
                    count_throw_away += mEst_offset[i][k];
                }
            }
//            printf("\n");

        }
//        printf("\n");
        vnEst_m1[i] = statistics;
        if(error_sum > 0) {
            vEst_offset_m1[i] = dt / (double)error_sum;
        } else {
            vEst_offset_m1[i] = 0;
        }
        vEst_stdev_m1[i] = sqrt(error_sq_sum);
    }


    printf("\n");

    int nCalibrated_m0 = 0;
    int nCalibrated_m1 = 0;
    int nEstimated_m0 = 0;
    int nEstimated_m1 = 0;
    double mean_error_m0 = 0;
    double mean_error_m1 = 0;

    for(int iTile=0; iTile < nTiles; iTile++) {
        double estimation_m0 = vEst_offset_m0[iTile];
        double estimation_m1 = vEst_offset_m1[iTile];
        double truth = 0.0;

        truth = vTiles[iTile].offset - vTiles[0].offset;

//        h_debug->Fill(truth);
        h_debug2->Fill(estimation_m1-truth);
        h2_debug->Fill(truth, estimation_m1-truth);

        double error_m0 = estimation_m0-truth;
        double error_m1 = estimation_m1-truth;

        if(METHOD == 0) {
            if(iTile < 5 || iTile > nTiles-6 ) {
                printf("Estimation for METHOD 0: %.3f, \t true value: %.3f, \t Error: %.3f \n", estimation_m0 , truth, error_m0);
            }
            if(iTile == 5) {
                printf("...\n");
                printf("...\n");
                printf("...\n");
            }
        } else if(METHOD == 1) {
            if(iTile < 5 || iTile > nTiles-6 || error_m1 > CALIBRATION_THR || error_m1 == 0) {
                printf("Est for tile %d (M 1): %.3f, \t true value: %.3f, \t Error: %.3f \t Statistics: %d \n", iTile, estimation_m1 , truth, error_m1, vnEst_m1[iTile]);
            }
            if(iTile == 5) {
                printf("...\n");
                printf("...\n");
                printf("...\n");
            }
        }

        if(not isnan(error_m0)) {
            if(METHOD == 0) 
                h_error->Fill(error_m0);

            mean_error_m0 += fabs(error_m0);
            nEstimated_m0++;
            if(fabs(error_m0) < CALIBRATION_THR) 
                nCalibrated_m0++;
        }
        if(not isnan(error_m1)) {
            if(METHOD == 1)
                h_error->Fill(error_m1);
        
            mean_error_m1 += fabs(error_m1);
            nEstimated_m1++;
            if(fabs(error_m1) < CALIBRATION_THR) 
                nCalibrated_m1++;
        }

        h2_timing_error->SetBinContent(iTile/(nTiles_phi)+1, iTile%(nTiles_phi) +1 , min(fabs(error_m1),1.0));
        if(not isnan(error_m1) && fabs(error_m1) < CALIBRATION_THR) {
            h2_calibrated->SetBinContent(iTile/(nTiles_phi)+1, iTile%(nTiles_phi) +1, 1);
        } else {
            h2_calibrated->SetBinContent(iTile/(nTiles_phi)+1, iTile%(nTiles_phi) +1, 0);
        }
//        printf("%d -> %d, %d \n", iTile, iTile/nTiles_phi, iTile % nTiles_phi);

    }

    printf("\n");
    printf("Last time was: %.9f s\n",Time/1.0e9);
    printf("\n");
    printf("In total there were %d events, %d single e events and %d internal conversion events.\n",nEvents, n_e_events, n_ic_events);
    printf("\n");
    printf("Tile combinations with 0 statistic: %d out of %d possible combinations \n",count_zero, nTiles*nTiles);
    printf("\n");
    printf("Total events thrown away due to noise reduction: %d \n", count_throw_away);
    printf("\n");

    printf("--- METHOD 0 ---\n");
    printf("Calibration events: %d\tout of %1.0e x 3 total events \n", nCalibration_event_m0, (double)nEvents);
    printf("Calibrated tiles: %d\tout of %d total tiles\n", nCalibrated_m0, nTiles);

    mean_error_m0 = mean_error_m0/nEstimated_m0;
    printf("Mean Error: %.4f \n", mean_error_m0);

    printf("\n");
    printf("--- METHOD 1 ---\n");
    printf("Calibration events: %d\tout of %1.0e x 3 total events \n", nCalibration_event_m1, (double)nEvents);
    printf("Calibrated tiles: %d\tout of %d total tiles\n", nCalibrated_m1, nTiles);

    mean_error_m1 = mean_error_m1/nEstimated_m1;
    printf("Mean Error: %.4f \n", mean_error_m1);

    if(not f_crosstalk && not f_noise_red && not f_noise) {
        vnCalibrated_m0.push_back(nCalibrated_m0);
        vnCalibrated_m1.push_back(nCalibrated_m1);

        vMean_error_m0.push_back(mean_error_m0);
        vMean_error_m1.push_back(mean_error_m1);
    } else if(not f_crosstalk && not f_noise_red && f_noise) {
        vnCalibrated_noise.push_back(nCalibrated_m1);
        vMean_error_noise.push_back(mean_error_m1);
    } else if(not f_crosstalk && f_noise_red && f_noise) {
        vnCalibrated_noise_red.push_back(nCalibrated_m1);
        vMean_error_noise_red.push_back(mean_error_m1);
    } else if(f_crosstalk && f_noise_red && f_noise) {
        vnCalibrated_ct.push_back(nCalibrated_m1);
        vMean_error_ct.push_back(mean_error_m1);
    }

    //delete matrices
    
    mEst_offset.clear();
    mEst_stdev.clear();
    mnEst.clear();
    mDt.clear();
    mDt_stdev.clear();

}


int main() {
    printf("Start Calibration Simulation...\n");

    TRint *theApp = new TRint("theApp",0,0);
    TRandom3 *rand = new TRandom3(0);

//    simulate(rand, false, false);

//    for(double analyse_time=MIN_TIME; analyse_time <= MAX_TIME; analyse_time += TIME_STEPS) {
//
//        printf("Analyse by using %3.3f seconds\n", analyse_time);
//        vAnalyse_time.push_back(analyse_time);
//        analyse(rand, analyse_time, false, false, false);
//    }

//    reset();

//    simulate(rand, false, true);

//    for(double analyse_time=MIN_TIME; analyse_time <= MAX_TIME; analyse_time += TIME_STEPS) {
//        printf("Analyse by using %3.3f seconds\n", analyse_time);
//        analyse(rand, analyse_time, false, false, true);
//    }

//    for(double analyse_time=MIN_TIME; analyse_time <= MAX_TIME; analyse_time += TIME_STEPS) {
//        printf("Analyse by using %3.3f seconds\n", analyse_time);
//        analyse(rand, analyse_time, false, true, true);
//    }

//    reset();

    
    simulate(rand, false, false);

//    for(double analyse_time=MIN_TIME; analyse_time <= MAX_TIME; analyse_time += TIME_STEPS) {
        double analyse_time = MAX_TIME;
        printf("Analyse by using %3.3f seconds\n", analyse_time);
        analyse(rand, analyse_time, false, false, false);
//    }


// ------>> ---- <<------ //
// ------>> Draw <<------ //
// ------>> ---- <<------ //

    TCanvas *c0 = new TCanvas("c0","c0",0,0,700,500);
    h_hits->GetXaxis()->SetTitle("Tile Number");
    h_hits->GetYaxis()->SetTitle("# Hits");
    h_hits->Draw();

    TCanvas *c10 = new TCanvas("c10","c10",0,0,700,500);
    h_error->GetXaxis()->SetTitle("dT_(est) - dT_(true) [ns]");
    h_error->GetYaxis()->SetTitle("# Entries");
    h_error->Draw();

    TCanvas *c1 = new TCanvas("c1","c1",0,0,700,500);
    c1->Divide(1,2);
    c1->cd(1);
    h2_statistics->Draw("COLZ");
    c1->cd(2);
    h2_rel_timing->Draw("COLZ");

    TCanvas *c2 = new TCanvas("c2","c2",0,0,700,500);
    c2->Divide(1,2);
    c2->cd(1);
    h2_timing_error->Draw("COLZ");
    c2->cd(2);
    h2_calibrated->Draw("COLZ");

    TGraph *g_timing_m0 = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vMean_error_m0[0]);
    TGraph *g_calibrated_m0 = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vnCalibrated_m0[0]);
    TGraph *g_timing_m1 = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vMean_error_m1[0]);
    g_timing_m1->GetXaxis()->SetTitle("Calibration Time [s]");
    g_timing_m1->GetYaxis()->SetTitle("Mean Error [ns]");
    g_timing_m1->SetLineColor(40);
    g_timing_m1->SetLineWidth(2);
    g_timing_m1->SetMarkerColor(40);
    TGraph *g_calibrated_m1 = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vnCalibrated_m1[0]);
    g_calibrated_m1->GetXaxis()->SetTitle("Calibration Time [s]");
    g_calibrated_m1->GetYaxis()->SetTitle("Number of Calibrated Tiles [ns]");
    g_calibrated_m1->SetLineColor(40);
    g_calibrated_m1->SetLineWidth(2);
    g_calibrated_m1->SetMarkerColor(40);
    TGraph *g_timing_noise = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vMean_error_noise[0]);
    g_timing_noise->SetTitle("with noise");
    g_timing_noise->SetLineColor(46);
    g_timing_noise->SetLineWidth(2);
    g_timing_noise->SetMarkerColor(46);
    TGraph *g_calibrated_noise = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vnCalibrated_noise[0]);
    g_calibrated_noise->SetTitle("with noise");
    g_calibrated_noise->SetLineColor(46);
    g_calibrated_noise->SetLineWidth(2);
    g_calibrated_noise->SetMarkerColor(46);
    TGraph *g_timing_noise_red = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vMean_error_noise_red[0]);
    g_timing_noise_red->SetTitle("with noise reduction");
    g_timing_noise_red->SetLineColor(42);
    g_timing_noise_red->SetLineWidth(2);
    g_timing_noise_red->SetMarkerColor(42);
    TGraph *g_calibrated_noise_red = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vnCalibrated_noise_red[0]);
    g_calibrated_noise_red->SetTitle("with noise reduction");
    g_calibrated_noise_red->SetLineColor(42);
    g_calibrated_noise_red->SetLineWidth(2);
    g_calibrated_noise_red->SetMarkerColor(42);
    TGraph *g_timing_ct = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vMean_error_ct[0]);
    g_timing_ct->SetTitle("with noise reduction and crosstalk");
    g_timing_ct->SetLineColor(30);
    g_timing_ct->SetLineWidth(2);
    g_timing_ct->SetMarkerColor(30);
    TGraph *g_calibrated_ct = new TGraph(vAnalyse_time.size(),&vAnalyse_time[0],&vnCalibrated_ct[0]);
    g_calibrated_ct->SetTitle("with noise reduction and crosstalk");
    g_calibrated_ct->SetLineColor(30);
    g_calibrated_ct->SetLineWidth(2);
    g_calibrated_ct->SetMarkerColor(30);

    TMultiGraph *mg_timing = new TMultiGraph("mg_timing", "Mean Error in Estimating Timing Offset");
    g_timing_m1->SetTitle("Mean Error in Estimating Timing Offset");
    mg_timing->Add(g_timing_m1, "A*L");
    mg_timing->Add(g_timing_m0, "*L");
    mg_timing->Add(g_timing_noise, "*L");
    mg_timing->Add(g_timing_noise_red, "*L");
    mg_timing->Add(g_timing_ct, "*L");
    TMultiGraph *mg_calibrated = new TMultiGraph("mg_calibrated", "Number of Calibrated Tiles");
    g_calibrated_m1->SetTitle("Number of Calibrated Tiles");
    mg_calibrated->Add(g_calibrated_m1, "A*L");
    mg_calibrated->Add(g_calibrated_m0, "*L");
    mg_calibrated->Add(g_calibrated_noise, "*L");
    mg_calibrated->Add(g_calibrated_noise_red, "*L");
    mg_calibrated->Add(g_calibrated_ct, "*L");


    TCanvas *c3 = new TCanvas("c3","c3",0,0,700,500);
    c3->Divide(1,2);
    c3->cd(1);
    mg_calibrated->Draw();
    c3->cd(1)->BuildLegend();
    c3->cd(2);
    mg_timing->Draw();
    c3->cd(2)->BuildLegend();

    TCanvas *c20 = new TCanvas("c20","c20",0,0,700,500);
    h_debug->Draw();
    TCanvas *c21 = new TCanvas("c21","c21",0,0,700,500);
    h2_debug->Draw();

    theApp->Run();
}
